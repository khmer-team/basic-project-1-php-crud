CREATE DATABASE php_blog;

CREATE TABLE user(
				 id INT PRIMARY KEY AUTO_INCREMENT,
				 username VARCHAR(50) UNIQUE,
				 email VARCHAR(100),
				 `password` VARCHAR(100),
				 `date`  DATE
);

CREATE TABLE post(
				 id INT PRIMARY KEY auto_increment,
				 user_id INT,
				 title VARCHAR(100),
				 body VARCHAR(250),
				 publisher VARCHAR(50),
				 `date` DATE,
				 CONSTRAINT FOREIGN KEY (user_id) references user(id) 
);
