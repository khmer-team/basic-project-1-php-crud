<?php include '../config/database.php'; ?>

<?php
//variable
$title = $content = '';
$titleErr = $contentErr = '';

$sql = "SELECT DISTINCT id, title, publisher, `date`, body FROM post";
$result = mysqli_query($conn, $sql);
//store result in an array
$post = mysqli_fetch_all($result, MYSQLI_ASSOC);



//form submited
if (isset($_POST['save'])) {
    //valid title
    if (empty($_POST['title'])) {
        $titleErr = 'Title is required.';
    } else {
        $title = filter_input(
            INPUT_POST,
            'title',
            FILTER_SANITIZE_FULL_SPECIAL_CHARS
        );
    }

    //valid content
    if (empty($_POST['content'])) {
        $contentErr = 'Content is required.';
    } else {
        $content = filter_input(
            INPUT_POST,
            'content',
            FILTER_SANITIZE_FULL_SPECIAL_CHARS
        );
    }

    $sql = "SELECT DISTINCT id, title, publisher, `date` FROM post";
    $result = mysqli_query($conn, $sql);
    //store result in an array
    $post = mysqli_fetch_all($result, MYSQLI_ASSOC);

    //store in database 
    if (empty($titleErr) && empty($contentErr)) {
        //add to database
        $sql = "INSERT INTO post (title, body, publisher, `date`) VALUES ('$title', '$content', 'admin', Now())";
        mysqli_query($conn, $sql);
        $title = $content = $titleErr = $contentErr = '';
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="../lib/tailwind.js"></script>
</head>

<body class="w-screen h-fit" id="body">
    <div id="main-page" <?php echo $titleErr ? 'style="filter: blur(2px);"' : null; ?>>
        <header class="flex justify-between p-10">
            <div>
                <img class="w-[50px]" src="../images/logo.jpg">
            </div>

            <div>
                <button id="allPost-btn" class="text-violet-900 mr-2 hover:underline ">All Posts</button>
                <button id="myPost-btn" class="text-violet-900">My Posts</button>
            </div>
            <div>
                <button class="bg-violet-900 text-white rounded-md font-medium px-4 py-2" id="create-new-post" name="create">Create a post</button>
            </div>
        </header>

        <main>
            <!--Data display header-->
            <div id="header" class="flex justify-between bg-violet-100 h-10 pt-2 mr-[8%] ml-[8%] pl-2 pr-2 rounded-md">
                <div class="font-bold md:w-14">Post ID</div>
                <div class="font-bold md:w-56">Post Title</div>
                <div class="font-bold md:w-22">Publisher</div>
                <div class="font-bold md:w-48">Publisher Date</div>
                <div class="font-bold">Action</div>
            </div>
            <!--Fetch data from database and display on webpage-->
            <?php if (empty($post)) :  'style="border-color: #EF4444;"' ?>
                <p> There is no post. </p>
            <?php endif; ?>

            <div id="myPost">
                <?php foreach ($post as $item) : ?>
                    <div class="row flex justify-between border-2 border-gray-200 ml-[8%] mr-[8%] pl-2 pr-2">
                        <div>
                            <?php echo $item['id'];  ?>
                        </div>
                        <div class="md:w-60 ml-[8%]">
                            <?php echo $item['title']; ?>
                        </div>
                        <div class="md:w-22 ml-[4%] sm:ml-[5%]">
                            <?php echo $item['publisher']; ?>
                        </div>
                        <div class=" md:w-48 ml-[4%] sm:ml-[5%]">
                            <?php echo $item['date']; ?>
                        </div>
                        <div class="sm:ml-2">
                            <button class="bg-violet-900 w-9 h-6 rounded-sm text-white m-1 text-sm">Edit</button>
                            <button class="bg-pink-400 w-12 h-6 rounded-sm text-white text-sm">Delete</button>
                        </div>
                    </div>

                <?php endforeach; ?>



            </div>

            <!-- Display All post -->
            <div id="allPost" class="">
                <?php foreach ($post as $item) : ?>
                    <div class="inline-block mr-[10px] px-[33px] py-[50px] last:mr-0">
                        <div class="flex flex-col">
                            <img class="w-[240px] flex" src="../images/flower.jpg">
                            <div class="w-[240px] inline-block">
                                <div class="text-[24px] font-semibold text-violet-900">
                                    <?php echo $item['title']; ?>
                                </div>
                                <div class="text-violet-900">
                                    <?php echo $item['body']; ?>
                                </div>
                                <a href="#" class="text-violet-900 font-bold">Read more</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>



        </main>

        <footer class="flex justify-center mt-[10%]">
            <button class="border border-black px-2 py-1">0</button>
            <button class="border border-black px-2 py-1">1</button>
            <button class="border border-black px-2 py-1">2</button>
            <button class="border border-black px-2 py-1">...</button>
            <button class="border border-black px-2 py-1">9</button>
        </footer>
    </div>

    <form class="hidden bg-white border-black border-2 absolute top-2/4 left-2/4 -translate-x-2/4 -translate-y-2/4 p-6" id="popup-page" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" <?php echo $titleErr ? 'style="display: inline-block;"' : null; ?>>
        <p class="flex justify-center text-[24px] font-medium  mb-[30px]">Creating a new post</p>
        <div class="flex justify-between gap-x-4">
            <label class="py-[10px] w-[70px]" for="title">Title
                <?php echo $titleErr ? '<p class="text-red-500 inline">*</p>' : null; ?>
            </label>
            <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500" id="title" name="title" placeholder="Please enter your post's title here" <?php echo $titleErr ? 'style="border-color: #EF4444;"' : null; ?> <?php echo !$titleErr ? 'value="' . $title . '"' : null; ?>>
        </div>
        <div class="w-[280px] ml-[23%] text-red-500 text-[12px] font-medium mb-[15px]">
            <?php echo $titleErr ?? null; ?>
        </div>

        <div class="flex justify-between gap-x-4">
            <label class="py-[10px] w-[70px]" for="content">content
                <?php echo $contentErr ? '<p class="text-red-500 inline">*</p>' : null; ?>
            </label>
            <textarea id="content" name="content" rows="4" placeholder="Pleasse write your post here..." class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500" <?php echo $contentErr ? 'style="border-color: #EF4444;"' : null; ?>>
            <?php echo isset($content) ? $content : null; ?>
            </textarea>
        </div>
        <div class="w-[280px] ml-[23%] text-red-500 text-[12px] font-medium">
            <?php echo $contentErr; ?>
        </div>

        <div class="flex justify-center gap-x-2 mt-[50px]">
            <button class="bg-green-500 text-white font-medium px-4 py-2" id="save" name="save">Save</button>
            <button class="bg-red-500 text-white font-medium px-4 py-2" id="cancel" name="cancel">Cancel</button>
        </div>
    </form>


    <script>
        // <!-- jQuery code to update the data dynamically -->
        src = "https://code.jquery.com/jquery-3.6.0.min.js"


        let createBtn = document.getElementById("create-new-post");
        let saveBtn = document.getElementById("save");
        let cancelBtn = document.getElementById("cancel");

        let popupPage = document.getElementById("popup-page"); //form
        let mainPage = document.getElementById("main-page")

        let myPost = document.getElementById("myPost");
        let myPostbtn = document.getElementById("myPost-btn");

        let allPost = document.getElementById("allPost");
        let allPostbtn = document.getElementById("allPost-btn");

        let header = document.getElementById("header");

        myPost.style.display = 'none';
        allPost.style.display = 'none';

        function updateData() {
            //send an AJAX request request to retrieve the new data
            $.ajax({
                url: 'get_data.php',
                success: function(data) {
                    //update the data container with the new data
                    $('#allPost').html(data);

                }
            });


        }
        //update the data every 5 seconds
        setInterval(updateData, 5000);

        allPostbtn.addEventListener("click", () => {
            header.style.display = 'none';
            //    document.body.style.color = 'rgb(107 33 168)';
            event.target.style.color = 'rgb(107 33 168)';
            //    event.target.style.color = 'bold';
            // updateData();
            allPost.style.display = 'inherit';
            myPost.style.display = 'none';
            allPostbtn.style.fontWeight = "bold";
            myPostbtn.style.fontWeight = "normal";

        })


        myPostbtn.addEventListener("click", () => {
            //    document.body.style.color = 'rgb(107 33 168)';
            event.target.style.color = 'rgb(107 33 168)';
            //    event.target.style.color = 'bold';
            myPost.style.display = 'inherit';
            allPost.style.display = 'none';
            myPostbtn.style.fontWeight = "bold";
            allPostbtn.style.fontWeight = "normal";
        })

        createBtn.addEventListener("click", () => {
            if (popupPage.style.display === "inline-block") {
                popupPage.style.display = "none";
                mainPage.style.filter = 'blur(0)';
            } else {
                popupPage.style.display = "inline-block";
                mainPage.style.filter = 'blur(2px)';
            }
        })

        cancelBtn.addEventListener("click", () => {
            <?php
            $title = $content = $titleErr = $contentErr = '';
            ?>
            popupPage.style.display = "none";
            mainPage.style.filter = 'blur(0)';
        })

        saveBtn.addEventListener("click", () => {
            mainPage.style.filter = 'blur(0)';
        })

        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
</body>

</html>