<?php include './config/database.php'; ?>

<?php
//set vars for empty values
    $name = $email = '';
    $nameErr = $emailErr = $emailClass = '';
    $password = $confirmPassword = '';
    $passwordErr = $confirmPasswordErr = '';

    $sql = "SELECT username FROM user WHERE username = '$name'";
   // $result = $conn->query($sql);
   $result = mysqli_query($conn, $sql);
    //form submit
if(isset($_POST['signup'])){
    //validate username
    if(empty($_POST['username'])){
        $nameErr = 'Username is required.';
    }
    else if(!empty($_POST['username'])){
        $name = filter_input(
            INPUT_POST,
            'username',
            FILTER_SANITIZE_FULL_SPECIAL_CHARS
          );

          if(strlen($name)>=3){
            if($result->num_rows > 0){ 
                //it returns a row where username is already taken
                $nameErr = 'Username is already taken.';
            }
          }
          else if(strlen($name)<3){
            $nameErr = 'Username must contains more than 3 characters.';
        } 
         }
    //validate email
    if (empty($_POST['email'])) {
        $emailErr = 'Email is required';
        
      } else {
        //filter email remove all illegal char
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        //validate email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = 'Invalid email address.';
        } 
      }

    //validate password
    if(empty($_POST['password'])) {
        $passwordErr = 'password is required!';
    } else {
        $password = filter_input(
            INPUT_POST, 
            'password', 
            FILTER_SANITIZE_FULL_SPECIAL_CHARS
        );

        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[\W]@', $password);
        $length = strlen($password);


        if(!$uppercase || !$lowercase || !$number || !$specialChars || $length < 8) {
            $passwordErr = 'Password should be at least 8 characters in length and should include at least one uppercase and lowercase, one number and special character.';
        }
    }

    //confirm password
    if(empty($_POST['confirmPassword'])) {
        $confirmPasswordErr = 'confirm password is required!';
    } else {
        $confirmPassword = filter_input(
            INPUT_POST,
            'confirmPassword',
            FILTER_SANITIZE_FULL_SPECIAL_CHARS
        );

        if(strcmp($password, $confirmPassword) != 0) {
            $confirmPasswordErr = 'The confirm password must be the same as the password which you have just inputted!';
        }

        //encrypt password 
        $hash = password_hash($confirmPassword, PASSWORD_DEFAULT);
    }

    //store into database
    if(empty($nameErr) && empty($emailErr) &&empty($passwordErr) && empty($confirmPasswordErr)){
        //add into database
        $sql = "INSERT INTO user (username, email, `password`, `date`) VALUES ('$name', '$email', '$hash', NOW())";
        mysqli_query($conn, $sql);

        //if sucess redirect to homepage
        }




}


?>
   
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="lib/tailwind.js"></script>

    <!-- <style>
        @layer components {
            .invalid-input {
                color: theme('color.red');
                font-size: theme('');
            }
        }
    </style> -->
</head>
<body class="w-screen h-screen">
    <div class="container border-2 w-screen h-screen">
        <nav class="flex bg-slate-100 border-gray-200 h-[70px] py-2.5 rounded">
            <div class="flex items-center justify-between mr-[79%] pl-3">
                <img class="w-[55px] h-[55px]  rounded-full" src="images/logo.jfif">
            </div>
            <button class="items-center h-10 mt-3 w-16 mr-2 md:ml-28 border-2 border-gray-500 p-2 text-sm text-gray-500 rounded-lg hover:bg-gray-200 focus:ring-2 focus:ring-gray-200">Log in</button>
        </nav>

        
        <form class="flex flex-col absolute top-1/2 left-1/2 translate-x-[-50%] translate-y-[-50%] gap-2"
        method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
            <div class="flex justify-between">
                <label class="flex items-center w-[160px] font-medium justify-end" for="username">Username
                    <?php echo $nameErr ? '<p class="text-red-500">*</p>' : null; ?>
                </label>
                <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500" 
                <?php echo $nameErr ? 'style="border-color-:  #EF4444;"' : null;?>
                type="text" id="username" name="username" placeholder="Please enter your username" >           
            </div>
            <div class="w-[280px] ml-[39%] text-red-500 text-[12px] font-medium mt-[-10px]">
             <?php echo $nameErr ?? null; ?>
            </div>

            <div class="flex justify-between">
                <label class="flex items-center justify-end font-medium w-[160px] " for="email">Email 
                    <?php echo $emailErr ? '<p class="text-red-500">*</p>' : null; ?>
                </label>
                <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500" 
                <?php echo $emailErr ? 'style="border-color: #EF4444;"' : null; ?>
                type="email" id="email" name="email" placeholder="Please enter your email">
            </div>
        
            <div class="w-[280px] ml-[39%] text-red-500 text-[12px] font-medium mt-[-10px]">
             <?php echo $emailErr ?? null; ?>
            </div> 
            

            <div class="flex justify-between">
                <label class="flex items-center text-[18px] font-medium justify-end w-[160px]" for="password">Password
                <?php echo $passwordErr ? '<p class="text-red-500">*</p>' : null; ?>
            </label>
                <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500"
                <?php echo $passwordErr ? 'style="border-color: #EF4444;"' : null; ?>
                type="password" id="password" name="password" placeholder="Please enter your password">
            </div>
            <div class="w-[280px] ml-[39%] text-red-500 text-[12px] font-medium mt-[-10px]">
                <?php echo $passwordErr ?? null; ?>
            </div>
        
            <div class="flex justify-between gap-4">
                <label class="flex items-center text-[18px] font-medium justify-end w-[160px]" for="confirmPassword">Confirm Password
                <?php echo $confirmPasswordErr ? '<p class="text-red-500">*</p>' : null; ?>
            </label>
                <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500" 
                <?php echo $confirmPasswordErr ? 'style="border-color: #EF4444;"' : null; ?>
                type="password" id="confirmPassword" name="confirmPassword" placeholder="Please enter your password again">
            </div>
            <div class="w-[280px] ml-[39%] text-red-500 text-[12px] font-medium mt-[-10px]">
                <?php echo $confirmPasswordErr ?? null; ?>
            </div>

            <button class="border-[1.5px] px-2 py-1 w-[280px] border-black bg-black text-white border-solid mt-[50px] ml-[39%] hover:border-black hover:text-black hover:bg-white" name="signup" id="sign-up">Sign up</button>
        </form>
    </div>
</body>
</html>
