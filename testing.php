<?php include './config/database.php'; ?>

<?php
//set vars for empty values
$name = $email = '';
$password = $confirmPassword = '';
$nameErr = $emailErr = $emailClass = '';
$passwordErr = $confirmPasswordErr = '';

$sql = "SELECT username FROM user WHERE username = '$name'";
// $result = $conn->query($sql);
$result = mysqli_query($conn, $sql);

//form submit
if (isset($_POST['signup'])) {
    //validate username
    if (empty($_POST['username'])) {
        $nameErr = 'Username is required.';
    } else if (!empty($_POST['username'])) {
        $name = filter_input(
            INPUT_POST,
            'username',
            FILTER_SANITIZE_FULL_SPECIAL_CHARS
        );

        if (strlen($name) >= 3) {
            if ($result->num_rows > 0) {
                //it returns a row where username is already taken
                $nameErr = 'Username is already taken.';
            }
        } else if (strlen($name) < 3) {
            $nameErr = 'Username must contains more than 3 characters.';
        }
    }

    //validate email
    if (empty($_POST['email'])) {
        $emailErr = 'Email is required';
    } else {
        //filter email remove all illegal char
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        //validate email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = 'Invalid email address.';
        }
    }

    //validate password
    if(empty($_POST['password'])) {
        $passwordErr = 'Password is required!';
    } else {
        $password = filter_input(
            INPUT_POST, 
            'password', 
            FILTER_SANITIZE_FULL_SPECIAL_CHARS
        );

        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[\W]@', $password);
        $length = strlen($password);


        if(!$uppercase || !$lowercase || !$number || !$specialChars || $length < 8) {
            $passwordErr = 'Password should be at least 8 characters in length and should include at least one uppercase and lowercase, one number and special character.';
        }
    }

    //validate confirm password
    if(empty($_POST['confirmPassword'])) {
        $confirmPasswordErr = 'Confirm password is required!';
    } else {
        $confirmPassword = filter_input(
            INPUT_POST,
            'confirmPassword',
            FILTER_SANITIZE_FULL_SPECIAL_CHARS
        );

        if(strcmp($password, $confirmPassword) != 0) {
            $confirmPasswordErr = 'The confirm password must be the same as the password which you have just inputted!';
        }

        //encrypt password 
        $hash = password_hash($confirmPassword, PASSWORD_DEFAULT);
    }

    //store into database
    if (empty($nameErr) && empty($emailErr) && empty($passwordErr) && empty($confirmPasswordErr)) {
        //add into database
        $sql = "INSERT INTO user (username, email, `password`, `date`) VALUES ('$name', '$email', '$hash', NOW())";
         
        if (mysqli_query($conn, $sql)) {
            //success
            header('Location: homepage.php');
        } else {
            echo 'Error: ' . mysqli_error($conn);
        }

        ///////////////////////
        //navigate to home page
        ///////////////////////
    }
}   
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Page</title>
    <script src="lib/tailwind.js"></script>
    <link href="https://use.fontawesome.com/releases/v6.2.1/css/all.css" rel="stylesheet">
</head>

<body class="w-screen h-screen">
    <div class="container border-2 w-screen h-screen">
        <nav class="flex bg-slate-100 border-gray-200 h-[70px] py-2.5 rounded">
            <div class="flex items-center justify-between mr-[79%] pl-3">
                <img class="w-[55px] h-[55px]  rounded-full" src="images/logo.jpg">
            </div>
            <button class="items-center h-10 mt-3 w-16 mr-2 md:ml-28 border-2 border-gray-500 p-2 text-sm text-gray-500 rounded-lg hover:bg-gray-200 focus:ring-2 focus:ring-gray-200">Log in</button>
        </nav>

        
        <form class="flex flex-col absolute top-1/2 left-1/2 translate-x-[-50%] translate-y-[-50%] gap-2"
        method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
            <div class="flex justify-between">
                <label class="flex items-center font-medium w-[160px] justify-end" for="username">Username
                <?php echo $nameErr ? '<p class="text-red-500">*</p>' : null; ?>
                </label>
                <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500" 
                <?php echo $nameErr ? 'style="border-color: #EF4444;"' : null; ?>
                <?php echo !$nameErr ? 'value="' . $name . '"' : null; ?>
                type="text" id="username" name="username" placeholder="Please enter your username">
            </div>
            <div class="w-[280px] ml-[40%] text-red-500 text-[12px] font-medium mt-[-10px]">
                <?php echo $nameErr ?? null; ?>
            </div>

            <div class="flex justify-between">
                <label class="flex items-center font-medium justify-end w-[160px]" for="email">Email
                <?php echo $emailErr ? '<p class="text-red-500">*</p>' : null; ?>
                </label>
                <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500"
                <?php echo $emailErr ? 'style="border-color: #EF4444;"' : null; ?>
                <?php echo !$emailErr ? 'value="' . $email . '"' : null; ?>
                type="email" id="email" name="email" placeholder="Please enter your email">
            </div>
            <div class="w-[280px] ml-[40%] text-red-500 text-[12px] font-medium mt-[-10px]">
                <?php echo $emailErr ?? null; ?>
            </div>

            <div class="flex justify-between relative">
                <label class="flex items-center font-medium justify-end w-[160px]" for="password">Password
                <?php echo $passwordErr ? '<p class="text-red-500">*</p>' : null; ?>
            </label>
                <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500"
                <?php echo $passwordErr ? 'style="border-color: #EF4444;"' : null; ?>
                <?php echo !$passwordErr ? 'value="' . $password . '"' : null; ?>
                type="password" id="password" name="password" placeholder="Please enter password">
                <i class="fa-regular fa-eye absolute top-4 right-2 z-10 hover:cursor-pointer" onclick="showPassword('password')"></i>
            </div>
            <div class="w-[280px] ml-[40%] text-red-500 text-[12px] font-medium mt-[-10px]">
                <?php echo $passwordErr ?? null; ?>
            </div>
        
            <div class="flex justify-between gap-4 relative">
                <label class="flex items-center font-medium justify-end w-[160px]" for="confirmPassword">Confirm Password
                    <?php echo $confirmPasswordErr ? '<p class="text-red-500">*</p>' : null; ?>
                </label>
                <input class="border px-4 py-[10px] w-[280px] border-slate-300 border-solid hover:border-blue-500 rounded-md focus:outline-blue-500" 
                    <?php echo $confirmPasswordErr ? 'style="border-color: #EF4444;"' : null; ?>
                    <?php echo !$confirmPasswordErr ? 'value="' . $confirmPassword . '"' : null; ?>
                    type="password" id="confirmPassword" name="confirmPassword" placeholder="Please enter password again">
                <i class="fa-regular fa-eye absolute top-4 right-2 z-10 hover:cursor-pointer" onclick="showPassword('confirmPassword')"></i>
            </div>
            <div class="w-[280px] ml-[40%] text-red-500 text-[12px] font-medium mt-[-10px]">
                <?php echo $confirmPasswordErr ?? null; ?>
            </div>

            <button class="border-[1.5px] px-2 py-1 w-[280px] border-black bg-black text-white border-solid mt-[50px] ml-[39%] hover:border-black hover:text-black hover:bg-white" name="signup" id="sign-up">Sign up</button>
        </form>
    </div>

    <script>
        function showPassword(x) {
            var password = document.getElementById(x);

            if(password.type === "password") {
                password.type = "text";
            } else {
                password.type = "password";
            }
        }
    </script>
</body>
</html>