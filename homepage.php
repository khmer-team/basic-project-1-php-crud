<?php include './config/database.php'?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <script src="./script.js"></script>
    <style>
        p{
            margin-top: 10px;
        }
        .container{
            text-align: center;
        }
        .container button{
            margin-left: 10px;
            width: 90px;
            height: 25px;
            color: white;
            background-color: gray;
            border-radius: 2px;
            margin-top: 5px;
        }
    </style>
    
</head>
<body onload="cookieExpired();">
    <div class="container">
        <h2>Welcome to our Home Page</h2>
        <p>You will be automatically logged out</p>
        <p>Thank you for your testing</p>
        <button id="logout" onclick="logout();">Logout</button>
    </div>
</body>
</html>